#!/Users/mmichel/app/bash/bin/bash
#!/bin/bash

TMP="$HOME/depot/ram/tmp"
mkdir "$TMP" 2>/dev/null
# test arg 1
if [ -z "$1" ] || [ ! -e "$1" ];then
	echo "Nom du binaire du projet manquant en argument"
	exit 1
fi

TEMOIN1=$TMP/test_bash
TEMOIN2=$TMP/test_sh42
BINP=$1
PROJECT=$(dirname $BINP)

mkdir "$TEMOIN1" "$TEMOIN2" 2>&-
echo PROJECT = $PROJECT
echo $TEMOIN1
echo $TEMOIN2

cp -rp -- "$PROJECT"/* "$TEMOIN1" 2>&-
cp -rp -- "$PROJECT"/* "$TEMOIN2" 2>&-
sync

i=0

# $1 envoyer sur stdin
shtest()
{
	i=$(($i+1))
	echo -e "\033[33m$i stdin = $1\033[0m"
	cd "$TEMOIN1"
	bash <<EOF &>$TMP/bash
$1
EOF
	
	cd "$TEMOIN2"
	./$BINP <<EOF &>$TMP/sh42
$1
EOF

	diff -a -y --suppress-common-lines \
		 $TMP/sh42 $TMP/bash
	diff -q -a -y --suppress-common-lines -r \
		 $TEMOIN1 $TEMOIN2
	#if [ $? -ne 0 ];then exit 1 ;fi
	rsync -a --delete-after $TEMOIN1/ $TEMOIN2/
	sync
}

# $2 = valeur attendu
shtestnobash()
{
	echo -e "\033[33mstdin = $1\033[0m"
	rm $TMP/sh42 $TMP/sh42_att 2>/dev/null
	
	cd "$TEMOIN2"
	./$BINP <<EOF &>$TMP/sh42
$1
EOF
	echo -ne "$2" > $TMP/sh42_att
	diff -a -y --suppress-common-lines \
		 $TMP/sh42 $TMP/sh42_att
}

# =
shtest 'ls && yy=uu && set | grep yy'
shtest 'ls | yy=uu && set | grep yy'

# $
shtest 'set gg "a e"
ls$gg-r'
shtest 'set gg "ae"
ls$gg-r'
shtest 'set gg " a e"
ls$gg-r'
shtest 'set gg " a"
ls$gg-r'
shtest 'set gg " a "
ls$gg-r'
shtest 'set gg "a "
ls$gg-r'
shtest 'set gg "a e "
ls$gg-r'
shtest 'set gg "'\''a e '\''"
ls$gg-r'

# \
shtest "ls -la tt\ oo"
shtest "mkdir 'tt\ oo'"
shtest "ls -la tt\ oo"
shtest "ls -la 'tt\ oo'"
shtest "ls -la 'tt oo'"
shtest "ls -la \"tt oo\""
#echo -e '\''' "\""
shtest 'echo -e '\''\'\'''\'''\'' "\""'

shtest "ls tt . 2 > f"
shtest "ls tt . 1 > f"
shtest "ls tt 2 > 10000000"
shtest "2 > f"
shtest "2 > 1"
shtest " > f"
shtest " > "

shtest "ls tt . 2> f"
shtest "ls tt . 1> f"
shtest "ls tt . &> f"
shtest "ls tt . 1> f 2> f2"
shtest "ls tt 1> f 2> f2"
shtest "ls 1> f 2> f2"
shtest "ls 1> f 2> f2 tt ff ."
shtest "ls 2> f 1> f2 tt ff ."
shtest "ls tt 2> 10000000"
shtest "ls tt 2> "

shtest "2> f"
shtest "2> 1"
shtest "> f"
shtest "> "

shtest "ls tt . 2>f"
shtest "ls tt . 1>f"
shtest "ls tt . &>f"
shtest "ls tt . 1>f 2>f2"
shtest "ls tt 1>f 2>f2"
shtest "ls 1>f 2>f2"
shtest "ls 1>f 2>f2 tt ff ."
shtest "ls tt . 2>&1"
shtest "ls . 2>&1"
shtest "ls tt 2>&1"
shtest "ls tt 2>&10000000"
shtest "ls tt >&10000000"
shtest "ls tt 2>10000000"
shtest "ls tt 2>&"
shtest "ls tt 2>"
shtest "ls tt >&"
shtest "ls tt 20>&"
shtest "2>&"
shtest "2>f"
shtest "f>&"
shtest "2>1"
shtest ">&"
shtest ">f"
shtest ">"
shtest "ls tt inc &>&-"

shtest "ls tt .git 2>f 1>&2"
shtest "ls tt .git 1>&2 2>f"
shtest "ls tt .git 2>f 1>&2 >&-"
shtest "ls tt .git >&- 2>f 1>&2"
shtest "ls tt .git >&- 1>&2 2>f"
shtest "ls tt . &>f"
shtest "ls &>> f tt ."

shtest "echo tt . 2>f"
shtest "echo tt . 1>f"
shtest "echo tt . &>f"
shtest "echo tt . 1>f 2>f2"
shtest "echo tt 1>f 2>f2"
shtest "echo 1>f 2>f2"
shtest "echo 1>f 2>f2 tt ff ."
shtest "echo tt . 2>&1"
shtest "echo . 2>&1"
shtest "echo tt 2>&1"
shtest "echo tt 2>&10000000"
shtest "echo tt >&10000000"
shtest "echo tt 2>10000000"
shtest "echo tt 2>&"
shtest "echo tt 2>"
shtest "echo tt >&"
shtest "echo tt 20>&"
shtest "echo tt inc &>&-"

shtest ">>"
shtest ">>&"
shtest ">>&-"
shtest ">>f"
shtest ">> g"
shtest ">>"
shtest "ls 1>>f 2>>f2 tt ff ."

#####
## Comportement different
shtest "ls tt . 1 > f 2 > f2"
shtest "ls tt 1 > f 2 > f2"
shtest "ls 1 > f 2 > f2"
shtest "ls 1 > f 2 > f2 tt ff ."

## < <<

shtest "ls tt . 2 < f"
shtest "ls tt . 1 < f"
shtest "ls tt 2 < 10000000"
shtest "2 < f"
shtest "2 < 1"
shtest " < f"
shtest " < "

shtest "ls tt . 2< f"
shtest "ls tt . 1< f"
shtest "ls tt . &< f"
shtest "ls tt . 1< f 2< f2"
shtest "ls tt 1< f 2< f2"
shtest "ls 1< f 2< f2"
shtest "ls 1< f 2< f2 tt ff ."
shtest "ls 2< f 1< f2 tt ff ."
shtest "ls tt 2< 10000000"
shtest "ls tt 2< "

shtest "2< f"
shtest "2< 1"
shtest "< f"
shtest "< "

shtest "ls tt . 2<f"
shtest "ls tt . 1<f"
shtest "ls tt . &<f"
shtest "ls tt . 1<f 2<f2"
shtest "ls tt 1<f 2<f2"
shtest "ls 1<f 2<f2"
shtest "ls 1<f 2<f2 tt ff ."
shtest "ls tt . 2<&1"
shtest "ls . 2<&1"
shtest "ls tt 2<&1"
shtest "ls tt 2<&10000000"
shtest "ls tt <&10000000"
shtest "ls tt 2<10000000"
shtest "ls tt 2<&"
shtest "ls tt 2<"
shtest "ls tt <&"
shtest "ls tt 20<&"
shtest "2<&"
shtest "2<f"
shtest "f<&"
shtest "2<1"
shtest "<&"
shtest "<f"
shtest "<"
shtest "ls tt inc &<&-"

shtest "ls tt .git 2<f 1<&2"
shtest "ls tt .git 1<&2 2<f"
shtest "ls tt .git 2<f 1<&2 <&-"
shtest "ls tt .git <&- 2<f 1<&2"
shtest "ls tt .git <&- 1<&2 2<f"
shtest "ls tt . &<f"
shtest "ls &<< f tt ."

shtest "echo tt . 2<f"
shtest "echo tt . 1<f"
shtest "echo tt . &<f"
shtest "echo tt . 1<f 2<f2"
shtest "echo tt 1<f 2<f2"
shtest "echo 1<f 2<f2"
shtest "echo 1<f 2<f2 tt ff ."
shtest "echo tt . 2<&1"
shtest "echo . 2<&1"
shtest "echo tt 2<&1"
shtest "echo tt 2<&10000000"
shtest "echo tt <&10000000"
shtest "echo tt 2<10000000"
shtest "echo tt 2<&"
shtest "echo tt 2<"
shtest "echo tt <&"
shtest "echo tt 20<&"
shtest "echo tt inc &<&-"

shtest "<<"
shtest "<<&"
shtest "<<&-"
shtest "<<f"
shtest "<< g"
shtest "<<"
shtest "ls 1<<f 2<<f2 tt ff ."
shtest "ls tt . 1 < f 2 < f2"
shtest "ls tt 1 < f 2 < f2"
shtest "ls 1 < f 2 < f2"
shtest "ls 1 < f 2 < f2 tt ff ."

# | && || ;
shtest "ls && echoo tt || ls .git | cat -e  && echo yy || ls ."
shtest "ls && echo tt || ls .git | cat -e  && echo yy || ls ."
shtest "echoo tt || ls | cat -e"
shtest "s && echo tt || cat f && echo yy"
shtest "echo tt | cat -e"
shtest "s | cat && echo tt || env"
shtest "ls ; ls .git"
shtest "ls ; ls truc"
shtest "ls truc ; ls .git"


# || && |

shtest 'lss | cat -e'
shtest 'ls | cat -e'

shtest 'lss | cat -e || echoo ii | cat -e'
shtest 'lss | cat -e && echoo ii | cat -e'
shtest 'lss | cat -e || echo ii | cat -e'
shtest 'lss | cat -e && echo ii | cat -e'
shtest 'ls | cat -e || echoo ii | cat -e'
shtest 'ls | cat -e && echoo ii | cat -e'
shtest 'ls | cat -e || echo ii | cat -e'
shtest 'ls | cat -e && echo ii | cat -e'

shtest 'lss | cat -e || echoo ii | cat -e && ls auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e || ls auteur | cat'
shtest 'ls | cat -e || echoo ii | cat -e && ls auteur | cat'
shtest 'ls | cat -e || echoo ii | cat -e || ls auteur | cat'
shtest 'lss | cat -e || echo ii | cat -e && ls auteur | cat'
shtest 'lss | cat -e || echo ii | cat -e || ls auteur | cat'
shtest 'ls | cat -e || echo ii | cat -e && ls auteur | cat'
shtest 'ls | cat -e || echo ii | cat -e || ls auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e && tttt auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e || tttt auteur | cat'

shtest "mkdir 'tt oo' && cd tt\ oo"

# |&
shtest "ls inc tt 2>f |& cat"

# <<
shtest "ls >f <<EOFF
truc
EOFF"
shtest "ls >f <<EOFF
truc
"
shtest "ls <<EOFF
"
shtest "ls <<
"
shtest "ls <<"
shtest "cat <<EOFF  >f
truc
EOFF"
shtest 'cat 2>f <<EOFF | cat -e                                                                                        
tr
EOFF'

shtest "ls >f << EOFF
truc
EOFF"
shtest "ls >f << EOFF
truc
"
shtest "ls << EOFF
"
shtest "ls <<
"
shtest "ls <<"
shtest "cat << EOFF  >f
truc
EOFF"
shtest 'cat 2>f << EOFF | cat -e                                                                                        
tr
EOFF'
shtest 'cat 2>f << 		    EOFF		    | cat -e                                                                                        
tr
EOFF'
shtest 'cat 2>f<<EOFF|cat -e                                                                                        
tr
EOFF'

# <<-
shtest "ls >f <<-EOFF
truc
EOFF"
shtest "ls >f <<-EOFF
truc
"
shtest "ls <<-EOFF
"
shtest "ls <<-
"
shtest "ls <<-"
shtest "cat <<-EOFF  >f
truc
EOFF"
shtest 'cat 2>f <<-EOFF | cat -e                                                                                        
tr
EOFF'

shtest "ls >f <<- EOFF
truc
EOFF"
shtest "ls >f <<- EOFF
truc
"
shtest "ls <<- EOFF
"
shtest "ls <<-
"
shtest "ls <<-"
shtest "cat <<- EOFF  >f
truc
EOFF"
shtest 'cat 2>f <<- EOFF | cat -e                                                                                        
tr
EOFF'

shtest 'cat 2>f <<- 		    EOFF		    | cat -e                                                                                        
tr
EOFF'
shtest 'cat 2>f<<-EOFF |cat -e                                                                                        
tr
EOFF'


shtest "ls >f << -EOFF
truc
EOFF"
shtest "ls >f << -EOFF
truc
"
shtest "ls << -EOFF
"
shtest "ls <<
"
shtest "ls <<"
shtest "cat << -EOFF  >f
truc
EOFF"
shtest 'cat 2>f << -EOFF | cat -e                                                                                        
tr
EOFF'

shtest 'cat 2>f << 		    -EOFF		    | cat -e                                                                                        
			tr
ads		t
EOFF'

shtest 'cat -e <<-EOFF                                                                                        
			tr
ads		t
rr	
EOFF'

# &
#shtest "ls tt . & > f"
# shtest "ls tt 2 > &"
# shtest "ls tt 2 > "
# shtest "ls tt  > &"
# shtest "ls tt 20 > &"
# shtest "2 > &"
# shtest "f > &"
# shtest " > &"

# shtest "ls tt . 2> &1"
# shtest "ls . 2> &1"
# shtest "ls tt 2> &1"
# shtest "ls tt 2> &10000000"
# shtest "ls tt > &10000000"
# shtest "ls tt 2> &"
# shtest "ls tt > &"
# shtest "ls tt 20> &"
# shtest "2> &"
# shtest "f> &"
# shtest "> &"
# shtest "ls tt . 2 > &1"
# shtest "ls . 2 > &1"
# shtest "ls tt 2 > &1"
# shtest "ls tt 2 > &10000000"
# shtest "ls tt  > &10000000"

shtest "cd .. && ls
pwd"
shtest "cd .. && ls &
pwd"
shtest "cd .. && ls ;
pwd"

shtest "tt=uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" 

shtestnobash "tt=uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\ntt=uu\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\ntt=uu\n"

shtest "tt=uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=ii\ntt=ii\n"

shtestnobash "tt=uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=ii\ntt=ii\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=ii\ntt=ii\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=ii\ntt=ii\n"

shtest "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtestnobash "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtest "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtestnobash "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

####
# export -n

shtest "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unset tt vv
set | sed -n 's/^tt=uu$/ko: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ko: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtestnobash "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unset tt vv
set | sed -n 's/^tt=uu$/ko: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ko: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtest "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt=ii vv=ss
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=ss$/ok: &/p'
env | sed -n 's/^vv=ss$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ss$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=$/ko: &/p'
env | sed -n 's/^tt=$/ko: &/p'
set | sed -n 's/^vv=$/ko: &/p'
env | sed -n 's/^vv=$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=ii\nok: tt=ii\nok: vv=ss\nok: vv=ss\nok: tt=ii\nok: vv=ss\n"

shtestnobash "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=ii vv=bb
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=bb$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=ii\nok: tt=ii\nok: vv=bb\nok: vv=bb\nok: tt=ii\nok: vv=bb\nok: tt=ii\nok: vv=bb\n"

shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt=ii vv=nn
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=nn$/ok: &/p'
env | sed -n 's/^vv=nn$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt/ko: &/p'
set | sed -n 's/^vv=nn/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii
ok: tt=ii\nok: tt=ii\nok: vv=nn\nok: vv=nn
ok: tt=ii\nok: vv=nn\n"

shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt=ii vv=oo
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=oo$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii
ok: tt=ii\nok: tt=ii\nok: vv=oo\nok: vv=oo
ok: tt=ii\nok: vv=oo
ok: tt=ii\nok: vv=oo\n"

shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\n"

shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export -n tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
"

shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

shtestnobash "
export -n tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
"

shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
export -n tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: vv=ii\n"

shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
export -n ttvv=oo
set | sed -n 's/^ttvv=oo/ok: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
" "ok: ttvv=oo\n"

shtestnobash "
set | sed -n 's/^ttvv=/ko: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
export -ttvv=ii 2>/dev/null
export -n -ttvv=ii 2>/dev/null
set | sed -n 's/^ttvv=/ko: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
"

exit 0
