#!/bin/sh

create_file_test()
{
	touch f-$1
	chmod $1 f-$1
	mkdir d-$1
	chmod $1 d-$1
}

create_test()
{
	mkdir tmp
	cd tmp

	j=0
	while [ $j -le 7 ]; do
		
		i=0
		while [ $i -le 7 ]; do
			create_file_test ${j}00$i
			i=$(($i+1))
		done
		i=1
		while [ $i -le 7 ]; do
			create_file_test ${j}0${i}0
			i=$(($i+1))
		done
		i=1
		while [ $i -le 7 ]; do
			create_file_test ${j}${i}00
			i=$(($i+1))
		done
		j=$(($j+1))
	done
	mkfifo fifo
	ln -s d-0000 d-lnk-denied
	ln -s f-0000 f-lnk-denied
	cd -
}
if [ ! -d "tmp" ];then
	create_test
fi

create_test_optmuti()
{
	mkdir -p tmp_multi/{n1/{n2-1,n2-2},n1-2/{n2-1,n2-1}}
	mkdir -p tmp_multi/{n1,n1-2}/*/n3
	touch tmp_multi/n1/{\*,n2-{1,2}}/sfih
	touch tmp_multi/n1/{\*,n2-{1,2}}/sfih
	touch tmp_multi/n1-2/{\*,n2-1}/{erwe,Asdg,Asdgg,025,026}
	touch -t 200805101024 tmp_multi/n1-2/n2-1/Asdgg
	touch -t 202005101024 tmp_multi/n1-2/n2-1/Asdgg
}

if [ ! -d "tmp_multi" ];then
	create_test_optmuti
fi

lstest()
{
	LINES=80
	COLUMNS=146
	okko=0
	if [ -z "$2" ];then
		tmp=.
	else
		tmp=$2
	fi
	rm -f ls ft ls2 ft2
	touch $tmp/{ft2,ls2,ft,ls}
	echo ""
	./ft_ls $1 2>$tmp/ft2 | less > $tmp/ft
	sed  's/ft_ls/ls/' $tmp/ft2 >$tmp/ft2.ren
	mv $tmp/ft2.ren $tmp/ft2
	ls $1 2>$tmp/ls2 | less > $tmp/ls
	diff $tmp/ft $tmp/ls -I io8log
	if [ $? -eq 0 ]; then
		echo "$1 ok stdout"
	else
		okko=1
		echo "$1 ko stdout"
	fi
	diff $tmp/ft2 $tmp/ls2 -I "usage" -I "ls: 4:"
	if [ $? -eq 0 ]; then
		echo "$1 ok stderr"
	else
		okko=1
		echo "$1 ko stderr"
	fi
	[[ $okko -eq 1 ]] && echo result $tmp/ft $tmp/ls $tmp/ft2 $tmp/ls2 \
		&& read -p"Erreur rencontre. Voulez vous continuer ? " -n 1
	rm -f -- $tmp/ft $tmp/ls $tmp/ft2 $tmp/ls2
}

all_lstest()
{
	lstest "-l $1" $2
	lstest "-R $1" $2
	lstest "-a $1" $2
	lstest "-r $1" $2
	lstest "-t $1" $2
	lstest "-lR $1" $2
	lstest "-lr $1" $2
	lstest "-la $1" $2
	lstest "-lt $1" $2
	lstest "-Rr $1" $2
	lstest "-Ra $1" $2
	lstest "-Rt $1" $2
	lstest "-ra $1" $2
	lstest "-rt $1" $2
	lstest "-ta $1" $2

	lstest "-l -R $1" $2
	lstest "-l -r $1" $2
	lstest "-l -a $1" $2
	lstest "-l -t $1" $2
	lstest "-R -r $1" $2
	lstest "-R -a $1" $2
	lstest "-R -t $1" $2
	lstest "-r -a $1" $2
	lstest "-r -t $1" $2
	lstest "-t -a $1" $2

	lstest "-l -R -r $1" $2
	lstest "-l -R -t $1" $2
	lstest "-l -R -a $1" $2
	lstest "-l -r -t $1" $2
	lstest "-l -r -a $1" $2
	lstest "-l -t -a $1" $2
	
	lstest "-lRr $1" $2
	lstest "-lRt $1" $2
	lstest "-lRa $1" $2
	lstest "-lrt $1" $2
	lstest "-lra $1" $2
	lstest "-lta $1" $2

	lstest "-l -R -r -a $1" $2
	lstest "-l -R -t -a $1" $2
	lstest "-l -R -a -t $1" $2
	lstest "-l -r -t -z $1" $2
	lstest "-l -r -a -R $1" $2
	lstest "-l -t -a -l $1" $2
}

all_lstest_bonus()
{
	lstest "-$1 $2" $3
	lstest "-R -$1 $2" $3
	lstest "-a -$1 $2" $3
	lstest "-l -$1 $2" $3
	lstest "-r -$1 $2" $3
	lstest "-t -$1 $2" $3
	lstest "-tr -$1 $2" $3
	lstest "-ta -$1 $2" $3
	lstest "-tra -$1 $2" $3
}

all_lstest "tmp/*"
all_lstest "" /tmp
all_lstest "." /tmp
all_lstest "/tmp ~/.Trash ~/Music"
all_lstest "Makefile auteur  ~/.zshrc"
all_lstest "Makefile auteur . tmp dflvgajneipr vn;kpWQ" /tmp
all_lstest "Makefile auteur . tmp" /tmp
all_lstest "Makefile auteur ~/.zshrc"
all_lstest "tmp"
all_lstest "tmp/d-0000"
all_lstest "tmp/d-7600"
all_lstest "tmp_multi"
all_lstest "/dev"
all_lstest "/var/run"

all_lstest_bonus A . /tmp
all_lstest_bonus S . /tmp
all_lstest_bonus d . /tmp
all_lstest_bonus f . /tmp
all_lstest_bonus n . /tmp
all_lstest_bonus g . /tmp
all_lstest_bonus u . /tmp


echo time ls
time ls -Rl ~ >/dev/null 2>&1
echo time ft_ls
time ./ft_ls -Rl ~ >/dev/null 2>&1

exit
