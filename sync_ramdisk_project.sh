#!/bin/bash

DEPOT=$HOME/depot
#SCP_REMOTE=user@ip:path/
SCP_REMOTE="sleepewig@83.156.214.177:~/tmp/"
SCP_OPT="-P 22000"
OPT=$1
PROJET=$2

if [ -z "${OPT}" ];then
	echo "Usage: $(basename $0) sav/res/mountres projet"
	exit 1
fi

if [ -z "${PROJET}" ];then
	echo "Nom du projet manquant."
	exit 1
elif [ "${OPT}" == "sav" ];then
	if [ ! -e $DEPOT/ram/${PROJET} ];then
		echo "Emplacement du projet invalide"
		exit 1
	fi
elif [ "${OPT}" == "res" ] || [ "${OPT}" == "mountres" ];then
	if [ ! -e $DEPOT/${PROJET} ];then
		echo "Emplacement du projet invalide"
		exit 1
	fi
fi

if [ "${OPT}" == "mountres" ];then
	NAME_RAMDISK="RamDisk"
	# ~600Mo
	RAM_SIZE="1165430"PROJET
	DEPOT_PROJET="$DEPOT/$PROJET"
	APP_EMACS="$HOME/app/Emacs.app"
	APP_BASH="$HOME/app/bash"

	# Creer/Format/mount ramdisk
	#mydev=/dev/disk2
	mydev=`hdiutil attach -nomount ram://$RAM_SIZE`
	echo $mydev
	newfs_hfs $mydev
	mkdir "$DEPOT"/ram 2>/dev/null
	mount -t hfs $mydev "$DEPOT"/ram
	# secour
	# mkdir /tmp/ram 2>/dev/null
	# mount -t hfs $mydev /tmp/ram

	for rep in $APP_EMACS $APP_BASH $DEPOT_PROJET
	do
		rsync -a --exclude ".fseventsd" "$rep" "$DEPOT"/ram
	done


elif [ "${OPT}" == "sav" ];then
	cd $DEPOT/ram/$PROJET \
		&& (
		make -s fclean
		rsync -a --delete-after --exclude ".fseventsd" \
				 "$HOME/depot/ram/${PROJET}/" \
				 "$HOME/depot/${PROJET}/"
		) \
		&& i=$(date +%Y-%m-%d_%H-%M-%S) \
		&& cd -P .. \
		&& tar cfJ ${PROJET}_${i}.tar.xz ${PROJET}/ \
		&& scp ${SCP_OPT} -r ${PROJET}_${i}.tar.xz ${SCP_REMOTE} \
		&& cd -

elif [ "${OPT}" == "res" ];then
	cd $DEPOT/$PROJET \
		&& make -s fclean \
		&& rsync -a --delete-after --exclude ".fseventsd" \
				 "$DEPOT/$PROJET" \
				 "$DEPOT/ram" \

else
	echo "Option: ${OPT}: inconnu."
	exit 1
fi

exit 0
