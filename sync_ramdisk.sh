#!/bin/sh

PROJECT=minishell

if [ -z "$PROJECT" ];then
	echo "Nom du projet manquant."
	return 1
fi

rsync -a --delete-after --exclude ".fseventsd" "$HOME/depot/ram/$PROJECT/" "$HOME/depot/$PROJECT/"
