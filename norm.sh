#!/bin/sh

if [ -z $@ ];then
	p="."
else
	p="$@"
fi

find $p -type f -iname "*.c" -exec awk '
BEGIN{
	NB_VAR = -1;
	NAME_FONC = 0;
	LIGNE_NAME_FONC = 0;
}
{
	# ligne depasse les 80 caracteres
	if (length($0) > 80)
	{
		print FILENAME" ligne: "FNR" "length($0)" > 80" ;
	}

	# # var
	# if (NB_VAR > -1 && FNR > START_FONC)
	# {
	# 	if ($0 ~ /^\t[a-zA-Z]*/)
	# 	{
	# 		# count init var
	# 		++NB_VAR;
	# 	}
	# 	else
	# 	{
	# 		if (NB_VAR > 5)
	# 		{
	# 			print FILENAME" var: "NB_VAR" > 5 in "NAME_FONC FNR ;
	# 			NB_VAR = -2 ;
	# 		}
	# 	}
	# }

	# fonction depasse les 25 lignes
	if (NAME_FONC == 0 && $0 ~ /^[a-zA-Z0-9 ]*\t+[a-z][a-z_0-9]*\(/)# recherche nom de fonction
	{
		i = match($0, /.*\t[a-z_0-9]*\(/);
		NAME_FONC = substr($0, RSTART, RLENGTH - RSTART)
		gsub(/.*\t/, "", NAME_FONC)
	}
	else if ($0 ~ /^\{$/)
	{
		START_FONC = FNR;
		NB_VAR = 0;
	}
	else if (NAME_FONC != 0 && $0 ~ /\/\/|\/\* .* \*\//)
	{
		print FILENAME" ligne: "FNR" comment" ;
		
	}
	else if ($0 ~ /^\}$/)
	{
		if (FNR - START_FONC > 26)
			print FILENAME" fonction: "FNR - START_FONC" > 25 "NAME_FONC ;
		NAME_FONC = 0 ;
		START_FONC = 0 ;
		NB_VAR = -1 ;
	}

	# debut / fin de ligne
	if (NAME_FONC != 0 && FNR > 6 + START_FONC && $0 ~ /^[\t ]*$/)
	{
		print FILENAME" ligne: "FNR" ligne vide" ;
	}
	else if ($0 ~ /[\t ]$/)
	{
		print FILENAME" ligne: "FNR" fin de ligne incorrecte" ;
	}

	if ($0 ~ /[ \t][ ]+/ && $1 !~ /^\/\*/)
		print FILENAME" ligne: "FNR" espace superflu" ;

}
END{
	if (NAME_FONC != 0)
		print FILENAME" saut de ligne a la fin"		
}
' {} \;
