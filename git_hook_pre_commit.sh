#!/bin/sh

exec 1>&2

FILE_LIST=$(git status --porcelain | sed -n 's/^[MA][MA]* *\(.*\.[ch]\)/\1/p')

if [ -n "$FILE_LIST" ];then

	norminette $FILE_LIST \
		| grep -v ^Norme:

	if [ $? == 0 ];then
		echo "Project 42 pas a la norme"
		exit 1
	fi
fi
