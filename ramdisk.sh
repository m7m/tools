#!/bin/sh

if [ -z "$1" ];then
	echo "Nom du projet manquant."
else
	PROJET=$1
fi

# config
NAME_RAMDISK="RamDisk"
RAM_SIZE="1165430"
DEPOT="$HOME/depot"
if [ -n "$PROJET" ];then
	DEPOT_PROJET="$DEPOT/$PROJET"
fi
APP_EMACS="$HOME/app/Emacs.app"
APP_BASH="$HOME/app/bash"

if [ ! -d "$DEPOT" ];then
	echo "Dossier du depot inexistant: '$DEPOT'"
	exit 1
fi

#mydev=/dev/disk2
mydev=`hdiutil attach -nomount ram://$RAM_SIZE`
echo $mydev
newfs_hfs $mydev
mkdir "$DEPOT"/ram 2>/dev/null
mount -t hfs $mydev "$DEPOT"/ram

for rep in $APP_EMACS $APP_BASH $DEPOT_PROJET
do
	rsync -a --exclude ".fseventsd" "$rep" "$DEPOT"/ram
done
